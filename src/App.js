import Boy from './pages/boy'
import Girl from './pages/girl'
import store from './store'
import { Provider } from 'react-redux'
function App() {
  return (
    <Provider store={store}>
      <div className="App" style={{position:"relative"}}>
        <div style={{position:"absolute",top:30}}> <Boy/></div>
        <div style={{position:"absolute",top:30,left:600}}> <Girl/></div>
      </div>
    </Provider>
  );
}

export default App;
