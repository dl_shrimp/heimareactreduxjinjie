//这样传值发现方便了，我不用管我具体传给哪个组件，哪个组件要用你从store里面拿就行
const initState = {
    status: false
}
export const  loveReducer = (state = initState, action) => {
    console.log(action);
    switch (action.type) {
        case 'send_love':
            //这里相当于直接给state重新赋值了，该不是initState.status改变它的值
            console.log('sendlove进入');
            
            return {
                status: true
            };
        case 'stop_love':
            console.log('stop_love进入');
            
            return {
                status: false
            };
        default: 
            return state;
    }
    
}