import React, { Component } from 'react'
import { connect } from 'react-redux'
import defaultImg from '../../static/boy.jpg'
import sendImg from '../../static/boyfs.jpg'



class Boy extends Component {

    state = {
        isSend: false
    }
    handleClick = () => {
        let {isSend} = this.state
        // if(!isSend) {
        //     this.props.sendLove()
        // }else{
        //     this.props.stopLove()
        // }
        !isSend ? this.props.sendLove() : this.props.stopLove()
        this.setState({
            isSend: !isSend
        })

    }
    render() {
        return (
            <>
                <img src={this.state.isSend ? sendImg : defaultImg} alt='' style={{height:300}}/>
                <div><button onClick={this.handleClick}>{this.state.isSend ? "停止发射":'发射爱心'}</button></div>
            </>
        )
    }
}
// export default Boy;
const mapDispatchToProps = (dispatch) => {
    //要有一个返回值，这个对象会返回给组件内部，可以通过this.props拿到
   return {
        sendLove: () => {
            dispatch({type: 'send_love'})
        },
        stopLove: () => {
            dispatch({type:'stop_love'})
        }
   }

}
//第一个参数是接收state的，第二个参数是发送action的
export default connect(null,mapDispatchToProps)(Boy);
